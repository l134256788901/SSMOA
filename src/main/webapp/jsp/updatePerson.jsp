<%@ page language="java" import="com.model.*,java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>UMS</title>
	
	<!-- 引入外部样式 -->
	<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>css/haha.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库 -->
	<script language="JavaScript" src="js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
		$(document).ready(function(){
			$(".click").click(function(){
			  $(".tip").fadeIn(200);
			});
			  
			$(".tiptop a").click(function(){
			  $(".tip").fadeOut(200);
			});
	
			$(".sure").click(function(){
			  $(".tip").fadeOut(100);
			});
	
			$(".cancel").click(function(){
			  $(".tip").fadeOut(100);
			});
		});
		function checkPhone(){
			var username=document.getElementById("username").value;
			if(username=="")
				{
				return false;
				}
			var phone=document.getElementById("phone").value;
	         var filter=/^1[3|4|5|7|8]\d{9}$/;  
	         if(filter.test(phone)){   
	            return true;   
	         }else{  
	        	 alert("所输入手机号码不符合规则！");
	            return false;  
	         }  
	    }  
	</script>
	
</head>

<body>
	<%OA_User person=(OA_User)session.getAttribute("person"); %>
	<!-- 上部：位置导航栏 -->
	<div class="place">
	   <span><%=person.getUsername() %>&nbsp;&nbsp;你好!欢迎访问用户权限管理系统!!</span>
    </div>
    
    <!-- 下部：内容列表区域 -->
    	
    	
    	
<form id="UpdatePerson" action="<%=basePath %>haha/UpdatePerson" method="post" class="basic-grey">
<label><span>昵称:</span><input type="text" name="username" id="username" value=<%=person.getUsername() %> />*</label>
<label><span>年龄:</span><input type="text" name="age" value=<%=person.getAge() %> /></label>
<label><span>性别:</span><%-- <input type="text" name="sex" value=<%=person.getSex() %> /> --%>
<select name="sex">
<%if(person.getSex().equals("女")){ %>
<option value="男">男</option>
<option value="女" selected>女</option>
<%}else{ %>
<option value="男" selected>男</option>
<option value="女">女</option>
<%} %>
</select>
</label>
<label><span>手机:</span><input type="text" name="phone" id="phone" value=<%=person.getPhone() %> />*</label>
<label><span>地址:</span><input type="text" name="address" value=<%=person.getAddress() %> /></label>
<label><span></span><input type="submit" value="保存数据" class="button" onclick="return checkPhone()" />&nbsp;&nbsp;
<a href="<%=basePath %>jsp/right.jsp"><button type="button" class="button">返回</button></a></label>
</form>
    	
    
    <!-- 弹出框：应该独立出去 -->
    <div class="tip">
   		<div class="tiptop">
   			<span>提示信息</span><a></a>
   		</div>
      	<div class="tipinfo">
	        <span><img src="<%=basePath%>images/ticon.png" /></span>
	        <div class="tipright">
		        <p>是否确认对信息的修改 ？</p>
		        <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
	        </div>
        </div>
        <div class="tipbtn">
	        <input name="" type="button"  class="sure" value="确定" />&nbsp;
	        <input name="" type="button"  class="cancel" value="取消" />
        </div>
   	</div>
    
    <!-- 实现变色效果 -->
    <script type="text/javascript">
		$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>