<%@ page language="java" import="com.model.*,java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>UMS</title>
	
	<!-- 引入外部样式 -->
	<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>css/haha.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库 -->
	<script language="JavaScript" src="js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
		$(document).ready(function(){
			$(".click").click(function(){
			  $(".tip").fadeIn(200);
			});
			  
			$(".tiptop a").click(function(){
			  $(".tip").fadeOut(200);
			});
	
			$(".sure").click(function(){
			  $(".tip").fadeOut(100);
			});
	
			$(".cancel").click(function(){
			  $(".tip").fadeOut(100);
			});
		});
	</script>
	
</head>

<body>
	
	<!-- 上部：位置导航栏 -->
	<%OA_User person=(OA_User)session.getAttribute("person"); %>
	<div class="place">
	    <!-- <span>位置：</span>
	    <ul class="placeul">
		    <li><a href="#">首页</a></li>
		    <li><a href="#">数据表</a></li>
		    <li><a href="#">基本内容</a></li>
	    </ul> -->
	    <span><%=person.getUsername() %>&nbsp;&nbsp;你好!欢迎访问用户权限管理系统!!</span>
    </div>
    
    <!-- 下部：内容列表区域 -->
   
    	
<%-- <h1>登录成功！！用户名为<%=person.getUsername() %></h1> --%>
<hr/>
<form action="" class="basic-grey">
<label><span>用户名:</span><input type="text" value=<%=person.getId() %> readonly /></label>
<label><span>密码:</span><input type="password" value=<%=person.getPassword() %> readonly /></label>
<label><span></span><a href="<%=basePath %>jsp/updatePassword.jsp"><button type="button" class="button">编辑数据</button></a></label>
</form>

 	
    
    <!-- 弹出框：应该独立出去 -->
    <div class="tip">
   		<div class="tiptop">
   			<span>提示信息</span><a></a>
   		</div>
      	<div class="tipinfo">
	        <span><img src="<%=basePath%>images/ticon.png" /></span>
	        <div class="tipright">
		        <p>是否确认对信息的修改 ？</p>
		        <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
	        </div>
        </div>
        <div class="tipbtn">
	        <input name="" type="button"  class="sure" value="确定" />&nbsp;
	        <input name="" type="button"  class="cancel" value="取消" />
        </div>
   	</div>
    
    <!-- 实现变色效果 -->
    <script type="text/javascript">
		$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>