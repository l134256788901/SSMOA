<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>UMS</title>
	
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
	<script language="JavaScript" src="js/jquery.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(function(){	
			//导航切换
			$(".menuson li").click(function(){
				$(".menuson li.active").removeClass("active");
				$(this).addClass("active");
			});
			
			$('.title').click(function(){
				var $ul = $(this).next('ul');
				$('dd').find('ul').slideUp();
				if($ul.is(':visible')){
					$(this).next('ul').slideUp();
				}else{
					$(this).next('ul').slideDown();
				}
			});
		});
	</script>
	
	<style type="text/css">
		body {
			background:#f0f9fd;
		}
	</style>
	
</head>

<body>
	
	<!-- 上部（显示一级菜单） -->
	<div class="lefttop">
		<span></span>系统管理
	</div>
	
	<!-- 下部（显示二级菜单） -->
	<dl class="leftmenu">
		<dd>
			<div class="title">
		    	<span><img src="images/leftico01.png"/></span>用户管理
		    </div>
				
		    <ul class="menuson">
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        	</ul>  
		</dd>
		
		<dd>
	    	<div class="title">
	    		<span><img src="images/leftico01.png" /></span>角色管理
	    	</div>
		    <ul class="menuson">
		        <li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
		    </ul>     
		</dd> 
		
		<dd>
	    	<div class="title">
	    		<span><img src="images/leftico01.png" /></span>权限管理
	    	</div>
		    <ul class="menuson">
		        <li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
        		<li><cite></cite><a href="test.jsp" target="rightFrame">测试页</a><i></i></li>
		    </ul>     
		</dd>
	</dl>	
</body>

</html>