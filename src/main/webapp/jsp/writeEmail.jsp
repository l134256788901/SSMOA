<%@ page language="java" import="com.model.*,java.util.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=basePath%>css/haha.css" rel="stylesheet" type="text/css" />
<title>写邮件</title>
<script type="text/javascript">
function checkTitle(){
	var title=document.getElementById("title").value;
	if(title=="")
		{
		alert("邮件标题不能为空！");
		return false;
		}
	else
		{
		return true;
		}
}  

</script>
</head>
<body>
<%List receivers=(List)request.getAttribute("receivers"); %>
<form action="writeEmail" method="post" enctype="multipart/form-data" class="basic-grey">

<label><span>收件人:</span><select name="receiver">
<%for(int i=0;i<receivers.size();i++){ %>
<option value=<%=((OA_User)receivers.get(i)).getId() %> >
<%=((OA_User)receivers.get(i)).getUsername() %>
</option>
<%} %>
</select><br/></label>
<label><span>邮件标题:</span><input type="text" id="title" name="title" />*</label>
<label><span>邮件内容:</span><!-- <input type="text" name="content" /><br/> -->
<textarea rows="1" cols="1" name="content"></textarea>
</label>
<label><span>上传附件:</span><input type="file" name="file1" /><br/></label>
<label><span></span><input type="hidden" name="id" value=<%=session.getAttribute("id") %> />
<input type="submit" value="发送邮件" onclick="return checkTitle()" /></label>
</form>
</body>
</html>