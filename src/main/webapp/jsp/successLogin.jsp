<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>用户权限管理系统</title>
	<link href="<%=basePath %>css/style.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库 -->
	<script language="JavaScript" src="<%=basePath %>js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
		$(function(){			
			//顶部导航切换
			$(".nav li a").click(function(){
				
				$(".nav li a.selected").removeClass("selected");
				
				$(this).addClass("selected");
				
			});	
		});
	</script>
		
	<!-- 定义内部样式 -->
	<style type="text/css">
		div#top{
			background:url(<%=basePath %>images/topbg.gif) repeat-x;
		}
	</style>
	
</head>

<!-- frameborder:设置是否有边框 -->
<frameset rows="88,*" frameborder="no" border="0" framespacing="0">
	
	<!-- 上部：导航栏 -->
	<!-- noresize:设置是否可以调整框架大小；scrolling：设置是否出现滚动条 -->
	 <frame src="<%=basePath %>jsp/top.jsp" name="topFrame" id="topFrame" title="topFrame" noresize="noresize" scrolling="no">
	
	<!-- 中部：左侧，菜单栏；右侧，内容栏（主页面） -->
	<frameset cols="187,*" border="0" framespacing="0" >

		<frame src="<%=basePath %>jsp/left.jsp" name="leftFrame" id="leftFrame" title="leftFrame" noresize="noresize" scrolling="no">

		<frame src="<%=basePath %>jsp/test.jsp" name="rightFrame" id="rightFrame" title="rightFrame">

	</frameset>
	
</frameset>

</html>