<%@ page language="java" import="com.model.*,java.util.*,java.text.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
<title>Insert title here</title>
<script>
function deleteEmail(i)
{
	alert("you have deleted!");
	document.getElementById("emailId").value=i;
	document.getElementById("deleteEmail").submit();
}
function startDelete()
{
	var tf=confirm("是否删除所选记录?");
	if(!tf)
		return false;
	return true;
}
</script>
</head>
<body>
<%List getEmail=(List)request.getAttribute("getEmail"); 
OA_User person=(OA_User)session.getAttribute("person"); %>
	<div class="place">
	    <!-- <span>位置：</span>
	    <ul class="placeul">
		    <li><a href="#">首页</a></li>
		    <li><a href="#">数据表</a></li>
		    <li><a href="#">基本内容</a></li>
	    </ul> -->
	    <span><%=person.getUsername() %>&nbsp;&nbsp;你好!欢迎访问用户权限管理系统!!</span>
    </div>
<table class="tablelist">
<tr>
<th>邮件标题</th>
<th>是否已读</th>
<th>时间</th>
<th>操作</th>
</tr>
<%
Email e;
for(int i=0;i<getEmail.size();i++)
	{
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 e=(Email)getEmail.get(i);%>
<tr>
<td><a href="<%=basePath%>haha/gotoReadEmail?emailid=<%=e.getId()%>"><%=e.getTitle() %></a></td>
<td><%=e.getReadstatus() %></td>
<td><%=sdf.format(e.getSendat()) %></td>
<td><form id="deleteEmail" action="deleteEmail" method="post">
<input type="hidden" name="id" value=<%=session.getAttribute("id") %> />
<input type="hidden" id="emailId" name="emailId" value=<%=e.getId() %> />
<input type="submit" id="删除" value="删除" onclick="return startDelete()" />
</form></td>
</tr>
<%} %>
</table>

</body>
</html>