<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>用户权限管理系统</title>
	
	<!-- 引入外部样式表 -->
	<link href="<%=basePath %>css/style.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入jquery库文件 -->
	<script language="JavaScript" src="<%=basePath %>js/jquery.js" type="text/javascript"></script>
	
	<script language="JavaScript" src="<%=basePath %>js/cloud.js" type="text/javascript"></script>
	
	<script language="javascript">
		// 指定loginbox的位置
		$(function(){
			$('.loginbox').css({
	    			'position':'absolute','left':($(window).width()-692)/2
	    		});
	    	
			$(window).resize(function(){  
		    	$('.loginbox').css({
		    		'position':'absolute','left':($(window).width()-692)/2
		    	});
		    }); 
		});  
	</script>
	
	<!-- 定义内部样式 -->
	<style type="text/css">
		body{
			background-color:#1c77ac; 
			background-image:url(images/light.png); 
			background-repeat:no-repeat; 
			background-position:center top; 
			overflow:hidden;
		}
	</style>
	
</head>
	
<body>
	
	<!-- 主页面背景图 -->
	<div id="mainBody">
      	<div id="cloud1" class="cloud"></div>
      	<div id="cloud2" class="cloud"></div>
	</div> 
     
    <!-- 登录页面头部 -->
    <div class="logintop">
    	 <span>欢迎访问用户权限管理管理系统</span>    
		 <ul>
		    <li><a href="#">帮助</a></li>
		    <li><a href="#">关于</a></li>
		 </ul>    
    </div>
    
    <!-- 登录页面体部 -->
    <div class="loginbody">
    	<span class="systemlogo"></span> 
    	<!-- 登录框 -->
    	<div class="loginbox">
    	<form action="haha/login" method="post">
    		<ul>
			    <li>
			    
			    	<input name="username" type="text" class="loginuser" value="用户名" onclick="JavaScript:this.value=''"/>
			    </li>
			    <li>
			    	<input name="password" type="password" class="loginpwd" value="" onclick="JavaScript:this.value=''"/>
			    </li>
			    <li>
			    	<!-- <input name="" type="button" class="loginbtn" value="登录"  onclick="javascript:window.location='main.jsp'"/> -->
			    	<!-- <label><input name="" type="checkbox" value="" checked="checked" />记住密码</label>
			    	<label><a href="#">忘记密码？</a></label> -->
			    	<input type="submit" value="登录" class="loginbtn" />
			    </li>
		    </ul>
		    </form>
    	</div>
    </div>
    
    <!-- 登录页面底部 -->
    <div class="loginbm">
    	Copyright &nbsp; &copy; &nbsp; 北大青鸟  &nbsp; 2016
    </div>
    
</body>

</html>