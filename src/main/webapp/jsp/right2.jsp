<%@ page language="java" import="com.model.*,java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>UMS</title>
	
	<!-- 引入外部样式 -->
	<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库 -->
	<script language="JavaScript" src="js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
		$(document).ready(function(){
			$(".click").click(function(){
			  $(".tip").fadeIn(200);
			});
			  
			$(".tiptop a").click(function(){
			  $(".tip").fadeOut(200);
			});
	
			$(".sure").click(function(){
			  $(".tip").fadeOut(100);
			});
	
			$(".cancel").click(function(){
			  $(".tip").fadeOut(100);
			});
		});
	</script>
	
</head>

<body>
	
	<!-- 上部：位置导航栏 -->
	<div class="place">
	    <span>位置：</span>
	    <ul class="placeul">
		    <li><a href="#">首页</a></li>
		    <li><a href="#">数据表</a></li>
		    <li><a href="#">基本内容</a></li>
	    </ul>
    </div>
    
    <!-- 下部：内容列表区域 -->
    <div class="rightinfo">
    	
    	<!-- 第一部分：查询条件 -->
    	<div class="tools">
	    	<ul class="toolbar">
		        <li class="click">
		        	<span><img src="<%=basePath%>images/t01.png" /></span>添加
		        </li>
		        <li class="click">
		        	<span><img src="<%=basePath%>images/t02.png" /></span>修改
		        </li>
		        <li>
		        	<span><img src="<%=basePath%>images/t03.png" /></span>删除
		        </li>
	        </ul>
	        <ul class="toolbar1">
	        	<li><span><img src="<%=basePath%>images/t05.png" /></span>设置</li>
	        </ul>
    	</div>
    	<%OA_User person=(OA_User)session.getAttribute("person"); %>
<h1>登录成功！！用户名为<%=person.getUsername() %></h1>
<hr/>
<p>个人信息</p>
<p>id:<%=person.getId() %></p>
<p>昵称:<%=person.getUsername() %></p>
<p>年龄:<%=person.getAge() %></p>
<p>性别:<%=person.getSex() %></p>
<p>手机:<%=person.getPhone() %></p>
<p>地址:<%=person.getAddress() %></p>
<a href="<%=basePath %>jsp/updatePerson.jsp"><button type="button">编辑数据</button></a>

    	<!-- 第二部分：表格信息 -->
    	<table class="tablelist">
    	
	    	<thead>
		    	<tr>
			        <th><input name="" type="checkbox" value="" checked="checked"/></th>
			        <th>编号<i class="sort"><img src="<%=basePath%>images/px.gif" /></i></th>
			        <th>标题</th>
			        <th>用户</th>
			        <th>籍贯</th>
			        <th>发布时间</th>
			        <th>是否审核</th>
			        <th>操作</th>
		        </tr>
	        </thead>
        
	        <tbody>
		        <tr>
			        <td><input name="" type="checkbox" value="" /></td>
			        <td>20130908</td>
			        <td>王金平幕僚：马英九声明字字见血 人活着没意思</td>
			        <td>admin</td>
			        <td>江苏南京</td>
			        <td>2013-09-09 15:05</td>
			        <td>已审核</td>
			        <td><a href="#" class="tablelink">查看</a>     <a href="#" class="tablelink"> 删除</a></td>
		        </tr> 
		        
		        <tr>
			        <td><input name="" type="checkbox" value="" /></td>
			        <td>20130907</td>
			        <td>温州19名小学生中毒流鼻血续：周边部分企业关停</td>
			        <td>uimaker</td>
			        <td>山东济南</td>
			        <td>2013-09-08 14:02</td>
			        <td>未审核</td>
			        <td><a href="#" class="tablelink">查看</a>     <a href="#" class="tablelink">删除</a></td>
		        </tr>
		        
		        <tr>
			        <td><input name="" type="checkbox" value="" /></td>
			        <td>20130906</td>
			        <td>社科院:电子商务促进了农村经济结构和社会转型</td>
			        <td>user</td>
			        <td>江苏无锡</td>
			        <td>2013-09-07 13:16</td>
			        <td>未审核</td>
			        <td><a href="#" class="tablelink">查看</a>     <a href="#" class="tablelink">删除</a></td>
		        </tr>
		        
		        <tr>
			        <td><input name="" type="checkbox" value="" /></td>
			        <td>20130906</td>
			        <td>社科院:电子商务促进了农村经济结构和社会转型</td>
			        <td>user</td>
			        <td>江苏无锡</td>
			        <td>2013-09-07 13:16</td>
			        <td>未审核</td>
			        <td><a href="#" class="tablelink">查看</a>     <a href="#" class="tablelink">删除</a></td>
		        </tr>
		        
		        <tr>
			        <td><input name="" type="checkbox" value="" /></td>
			        <td>20130906</td>
			        <td>社科院:电子商务促进了农村经济结构和社会转型</td>
			        <td>user</td>
			        <td>江苏无锡</td>
			        <td>2013-09-07 13:16</td>
			        <td>未审核</td>
			        <td><a href="#" class="tablelink">查看</a>     <a href="#" class="tablelink">删除</a></td>
		        </tr>
	        </tbody>
    	</table>
    	
    	<!-- 第三部分:分页信息 -->
    	<div class="pagin">
    		<div class="message">
    			共<i class="blue">1256</i>条记录，当前显示第&nbsp;
    			<i class="blue">2&nbsp;</i>页
    		</div>
	        <ul class="paginList">
		        <li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
		        <li class="paginItem"><a href="javascript:;">1</a></li>
		        <li class="paginItem current"><a href="javascript:;">2</a></li>
		        <li class="paginItem"><a href="javascript:;">3</a></li>
		        <li class="paginItem"><a href="javascript:;">4</a></li>
		        <li class="paginItem"><a href="javascript:;">5</a></li>
		        <li class="paginItem more"><a href="javascript:;">...</a></li>
		        <li class="paginItem"><a href="javascript:;">10</a></li>
		        <li class="paginItem"><a href="javascript:;"><span class="pagenxt"></span></a></li>
	        </ul>
    	</div>
    </div>
    
    <!-- 弹出框：应该独立出去 -->
    <div class="tip">
   		<div class="tiptop">
   			<span>提示信息</span><a></a>
   		</div>
      	<div class="tipinfo">
	        <span><img src="<%=basePath%>images/ticon.png" /></span>
	        <div class="tipright">
		        <p>是否确认对信息的修改 ？</p>
		        <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
	        </div>
        </div>
        <div class="tipbtn">
	        <input name="" type="button"  class="sure" value="确定" />&nbsp;
	        <input name="" type="button"  class="cancel" value="取消" />
        </div>
   	</div>
    
    <!-- 实现变色效果 -->
    <script type="text/javascript">
		$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>