<%@ page language="java" import="java.util.*,com.model.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>UMS</title>
	
	<!-- 引入外部样式 -->
	<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库-->
	<script language="JavaScript" src="<%=basePath%>js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
	
		$(function(){	
			//导航切换
			$(".menuson li").click(function(){
				$(".menuson li.active").removeClass("active");
				$(this).addClass("active");
			});
			
			$('.title').click(function(){
				var $ul = $(this).next('ul');
				$('dd').find('ul').slideUp();
				if($ul.is(':visible')){
					$(this).next('ul').slideUp();
				}else{
					$(this).next('ul').slideDown();
				}
			});
		});
		
		function gotoWriteEmail()
		{
			document.getElementById("gotoWriteEmail").submit();
		}
	</script>
	
	<!-- 定义内部样式 -->
	<style type="text/css">
		body {
			background:#f0f9fd;
		}
	</style>
	
</head>

<body>
	
	<!-- 上部（显示一级菜单） -->
	<div class="lefttop">
		<span></span>工作台
	</div>
	<%
				OA_User person = (OA_User) session.getAttribute("person");
	%>
	<!-- 下部（显示二级菜单） -->
	<dl class="leftmenu">
		<dd>
			<div class="title">
		    	<span><img src="<%=basePath%>images/leftico01.png"/></span>信息管理
		    </div>
			
			<ul class="menuson">
        		
        		<li><cite></cite><a href="<%=basePath%>jsp/right.jsp" target="rightFrame">个人信息</a><i></i></li>
        	</ul>  
		</dd>
		<dd>
			<div class="title">
		    	<span><img src="<%=basePath%>images/leftico01.png"/></span>邮件管理
		    </div>
			<ul class="menuson">
        		<li><cite></cite><a href="<%=basePath %>haha/gotoWriteEmail" target="rightFrame">写邮件</a><i></i></li>
        		<li><cite></cite><a href="<%=basePath %>haha/gotoGetEmail" target="rightFrame">收邮件</a><i></i></li>
        		<li><cite></cite><a href="<%=basePath %>haha/gotoRubbishEmail" target="rightFrame">垃圾邮件</a><i></i></li>
        	</ul>  
		</dd>
		
		<dd>
	    	<div class="title">
	    		<span><img src="<%=basePath%>images/leftico01.png" /></span>考勤管理
	    	</div>
		    <ul class="menuson">
		        
        		 <li><cite></cite><a href="<%=basePath %>haha/gotoAllVacation" target="rightFrame">休假信息列表</a><i></i></li>
        		 <%if(Integer.parseInt(person.getJobno())==1){ %>
        		<li><cite></cite><a href="<%=basePath %>haha/gotoShenhe"  target="rightFrame">审核休假</a><i></i></li>
        		<%} %>
		    </ul>     
		</dd> 
		
		<dd>
	    	<div class="title">
	    		<span><img src="<%=basePath%>images/leftico01.png" /></span>权限管理
	    	</div>
		    <ul class="menuson">
		      <li><cite></cite><a href="<%=basePath%>jsp/password.jsp" target="rightFrame">个人账户</a><i></i></li>
		        <%if(Integer.parseInt(person.getJobno())==1){ %>
        		<li><cite></cite><a href="<%=basePath %>haha/gotoAccounts" target="rightFrame">管理账户</a><i></i></li>
        		<%} %>
		    </ul>     
		</dd>
	</dl>	
</body>

</html>