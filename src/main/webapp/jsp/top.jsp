<%@ page language="java" import="com.model.*,java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>UMS</title>
	
	<!-- 引入外部样式 -->
	<link href="<%=basePath %>css/style.css" rel="stylesheet" type="text/css" />
	
	<!-- 引入Jquery库 -->
	<script language="JavaScript" src="<%=basePath %>js/jquery.js" type="text/javascript"></script>
	
	<!-- 定义内部动作 -->
	<script type="text/javascript">
		$(function(){			
			//顶部导航切换
			$(".nav li a").click(function(){
				
				$(".nav li a.selected").removeClass("selected");
				
				$(this).addClass("selected");
				
			});	
		});
	</script>
		<script language="javascript">
function test()
{
    var userName = parent.frames("rightFrame").document.all.username.value;
    alert(userName);
}
function startOut()
{
	var tf=confirm("是否退出登录?");
	if(!tf)
		return false;
	return true;
}
</script>
	<!-- 定义内部样式 -->
	<style type="text/css">
		body{
			background:url(<%=basePath %>images/topbg.gif) repeat-x;
		}
	</style>
	
</head>

<body>
	
	<!-- 左侧部分 -->
	<div class="topleft">
    	<img src="<%=basePath %>images/logo.png" title="用户权限管理系统" />
    </div>
	
	<!-- 中间部分（一级菜单--应该从数据库中读取并加载） -->
	<ul class="nav">
		<li>
			<!-- 加载工作台权限菜单（二级菜单） -->
			<a href="left.jsp" target="leftFrame" class="selected">
				<img src="<%=basePath %>images/icon01.png" title="工作台" />
				<h2>工作台</h2>
			</a>
		</li>
		
    	<li>
    		<!-- 加载系统管理权限菜单（二级菜单） -->
    		<a href="left-sysmanager.jsp" target="leftFrame">
    			<img src="<%=basePath%>images/icon06.png" title="系统管理" />
    			<h2>系统管理</h2>
    		</a>
    	</li>
    </ul>
    
    <!-- 右侧部分 -->
    <div class="topright">    
    	<!-- 公共导航 -->
    	<ul>
		    <li>
		    	<span>
		    		<img src="<%=basePath%>images/help.png" title="帮助"  class="helpimg"/>
		    	</span>
		    	<a href="#">帮助</a>
		    </li>
		    <li>
		    	<a href="#">关于</a>
		    </li>
		    <li>
		    	<a href="<%=basePath%>haha/logout" target="_parent" onclick="return startOut()">退出</a>
		    </li>
	    </ul>
	    <%OA_User ou=(OA_User)session.getAttribute("person"); %>
	    <!-- 当前登录用户信息 -->
	    <div class="user">
		  <%--   <span><a href="#" onclick="test()"><%=session.getAttribute("username") %></a></span>
		    <i>消息</i>
		    <b>5</b> --%>
	    </div>   
    </div>
</body>

</html>