package com.controller;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.model.Haha;
import com.dao.HahaMapper;

@Controller
@RequestMapping("/user")
public class UserController {
	@Resource
	private HahaMapper hm;

	@RequestMapping("/showUser")
	public String toIndex(HttpServletRequest request, Model model) {
		// int userId = Integer.parseInt(request.getParameter("id"));
		// User user = this.userService.getUserById(userId);
		// model.addAttribute("user", user);
		Haha hh=hm.selectByPrimaryKey(new BigDecimal(1));
		request.setAttribute("haha", hh);
		System.out.println(hh.getName());
		return "haha";
	}
}