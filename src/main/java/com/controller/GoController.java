package com.controller;

import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.*;

import eu.bitwalker.useragentutils.UserAgent;

import com.dao.*;

@Controller
@RequestMapping("/haha")
public class GoController {
	@Resource
	private HahaMapper hm;
	@Resource
	private OA_UserMapper oum;
	@Resource
	private EmailMapper em;
	@Resource
	private VacationMapper vm;
	@RequestMapping("/gotoWriteEmail")
	public String gotoWriteEmail(HttpServletRequest request, Model model) 
	{
		HttpSession hs=request.getSession();
		//Long id=Long.parseLong(request.getParameter("id"));
		List L=oum.selectByExample(new OA_UserExample());
		//request.setAttribute("id", id);
		request.setAttribute("receivers", L);
		return "writeEmail";
	}
	@RequestMapping("/gotoGetEmail")
	public String gotoGetEmail(HttpServletRequest request, Model model) 
	{
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=em.selectByExample(new EmailExample());
		//request.setAttribute("id", id);
		List getEmail=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			Email e=(Email)L.get(i);
			if(e.getReceiverId()==id&&(e.getDeleted()==null||e.getDeleted()==(short)0))
			{	
				e.setReceivestatus("yes");
				em.updateByPrimaryKey(e);
				getEmail.add(e);
			}
		}
		request.setAttribute("getEmail", getEmail);
		return "getEmail";
	}
	@RequestMapping("/gotoRubbishEmail")
	public String gotoRubbishEmail(HttpServletRequest request, Model model) 
	{
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=em.selectByExample(new EmailExample());
		//request.setAttribute("id", id);
		List rubbishEmail=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			Email e=(Email)L.get(i);
			if(e.getReceiverId()==id&&(e.getDeleted()==(short)1))
				rubbishEmail.add(e);
		}
		request.setAttribute("rubbishEmail", rubbishEmail);
		return "rubbishEmail";
	}
	@RequestMapping("/gotoQingJia")
	public String gotoQingJia(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=oum.selectByExample(new OA_UserExample());
		//request.setAttribute("id", id);
		List L2=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			OA_User ou=(OA_User)L.get(i);
			if(ou.getJobno().equals("1"))
			{
				L2.add(ou);
			}
		}
		request.setAttribute("managers", L2);
		return "qingJia";
	}
	@RequestMapping("/gotoAllVacation")
	public String gotoAllVacation(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=vm.selectByExample(new VacationExample());
		List L2=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			Vacation v=(Vacation)L.get(i);
			if(v.getSupplierId().equals(id))
			{
				L2.add(v);
			}
		}
		request.setAttribute("allVacation", L2);
		return "allVacation";
	}
	@RequestMapping("/gotoShenhe")
	public String gotoShenhe(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=vm.selectByExample(new VacationExample());
		List L2=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			Vacation v=(Vacation)L.get(i);
			if(v.getManagerId().equals(id))
			{
				L2.add(v);
			}
		}
		request.setAttribute("allVacation", L2);
		return "shenhe";
	}
	@RequestMapping("/gotoTongguo")
	public String gotoTongguo(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		Long vid=Long.parseLong(request.getParameter("vid"));
		Vacation v=vm.selectByPrimaryKey(vid);
		request.setAttribute("Vacation", v);
		return "tongguo";
	}
	@RequestMapping("/gotoAccounts")
	public String gotoAccounts(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		//request.setAttribute("id", id);
		List<OA_User> L=oum.selectByExample(new OA_UserExample());
		request.setAttribute("Accounts", L);
		return "Accounts";
	}
	@RequestMapping("/gotoAddAccount")
	public String gotoAddAccount(HttpServletRequest request, Model model) 
	{
		
		return "AddAccount";
	}
	@RequestMapping("/gotoReadEmail")
	public String gotoReadEmail(HttpServletRequest request, Model model) 
	{
		Long emailid=Long.parseLong((String)request.getParameter("emailid"));
		Email e=em.selectByPrimaryKey(emailid);
		e.setReadstatus("yes");
		em.updateByPrimaryKey(e);
		request.setAttribute("readEmail",e);
		return "readEmail";
	}
	@RequestMapping("/gotoUpdatePerson")
	public String gotoUpdatePerson(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		//System.out.println("the person id will update is "+id);
		//OA_User ou=oum.selectByPrimaryKey(id);
		//request.setAttribute("person",ou);
		return "updatePerson";
	}
	
    @RequestMapping(value="/download")
    public void download(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
    		    //声明本次下载状态的记录对象
    	 String fileName=request.getParameter("filename"); 
    	 ServletContext sc = request.getSession().getServletContext();
    	 String filePath =sc.getRealPath("/");
    	 System.out.println(filePath+fileName);
    		   // DownloadRecord downloadRecord = new DownloadRecord(fileName, filePath, request);
    		    //设置响应头和客户端保存文件名
    		    //response.setCharacterEncoding("utf-8");
    		    //response.setContentType("multipart/form-data");
    	 String encodedFileName = null;
         String userAgentString = request.getHeader("User-Agent");
         String browser = UserAgent.parseUserAgentString(userAgentString).getBrowser().getGroup().getName();
         if(browser.equals("Chrome") || browser.equals("Internet Exploer") || browser.equals("Safari")) {
             encodedFileName = URLEncoder.encode(fileName,"utf-8").replaceAll("\\+", "%20");
         } else {
             encodedFileName = MimeUtility.encodeWord(fileName);
         }
         response.setHeader("Content-Disposition", "attachment;fileName=\"" + encodedFileName + "\"");
    		  //  response.setHeader("Content-Disposition", "attachment;fileName=" + java.net.URLEncoder.encode(fileName, "Unicode"));
    		    //用于记录以完成的下载的数据量，单位是byte
    		    long downloadedLength = 0l;
    		    try {
    		        //打开本地文件流
    		        InputStream inputStream = new FileInputStream(filePath+fileName);
    		        //激活下载操作
    		        OutputStream os = response.getOutputStream();

    		        //循环写入输出流
    		        byte[] b = new byte[2048];
    		        int length;
    		        while ((length = inputStream.read(b)) > 0) {
    		            os.write(b, 0, length);
    		            downloadedLength += b.length;
    		        }

    		        // 这里主要关闭。
    		        os.close();
    		        inputStream.close();
    		    } catch (Exception e){
    		        //downloadRecord.setStatus(DownloadRecord.STATUS_ERROR);
    		        throw e;
    		    }
    		   // downloadRecord.setStatus(DownloadRecord.STATUS_SUCCESS);
    		   // downloadRecord.setEndTime(new Timestamp(System.currentTimeMillis()));
    		    //downloadRecord.setLength(downloadedLength);
    		    //存储记录
    		}
	
}