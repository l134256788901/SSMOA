package com.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.model.*;
import com.dao.*;

@Controller
@RequestMapping("/haha")
public class UpdateController {
	@Resource
	private HahaMapper hm;
	@Resource
	private OA_UserMapper oum;
	@Resource
	private EmailMapper em;
	@Resource
	private VacationMapper vm;
	@RequestMapping("/UpdatePerson")
	public String updatePerson(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		Long id=(Long) request.getSession().getAttribute("id");
		OA_User ou=oum.selectByPrimaryKey(id);
		String username=request.getParameter("username");
		Short age=Short.parseShort(request.getParameter("age"));
		String sex=request.getParameter("sex");
		String phone=request.getParameter("phone");
		String address=request.getParameter("address");
		ou.setAddress(address);
		ou.setAge(age);
		ou.setPhone(phone);
		ou.setSex(sex);
		ou.setType("yy");
		ou.setUsername(username);
		oum.updateByPrimaryKey(ou);
		//request.setAttribute("person",ou);
		request.getSession().setAttribute("person",ou);
		HttpSession session=request.getSession();
		session.setAttribute("username", ou.getUsername());
		//return "successLogin";
		return "right";
	}
	@RequestMapping("/UpdatePassword")
	public String updatePassword(HttpServletRequest request, Model model) 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		Long id=(Long) request.getSession().getAttribute("id");
		OA_User ou=oum.selectByPrimaryKey(id);
		String password=request.getParameter("password");
		ou.setPassword(password);
		oum.updateByPrimaryKey(ou);
		//request.setAttribute("person",ou);
		request.getSession().setAttribute("person",ou);
		HttpSession session=request.getSession();
		session.setAttribute("username", ou.getUsername());
		//return "successLogin";
		return "password";
	}
	@RequestMapping("/return1")
	public String return1(HttpServletRequest request, Model model) 
	{
		Long id=Long.parseLong(request.getParameter("id2"));
		OA_User ou=oum.selectByPrimaryKey(id);
		request.setAttribute("person",ou);
		return "successLogin";
	}
	@RequestMapping("/pass1")
	public String pass1(HttpServletRequest request, Model model) 
	{
		Long vid=Long.parseLong(request.getParameter("vid"));
		Vacation v=vm.selectByPrimaryKey(vid);
		v.setStatus("yes");
		vm.updateByPrimaryKey(v);
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=vm.selectByExample(new VacationExample());
		List L2=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			v=(Vacation)L.get(i);
			if(v.getManagerId().equals(id))
			{
				L2.add(v);
			}
		}
		request.setAttribute("allVacation", L2);
		return "shenhe";
	}
	@RequestMapping("/refuse1")
	public String refuse1(HttpServletRequest request, Model model) 
	{
		Long vid=Long.parseLong(request.getParameter("vid"));
		Vacation v=vm.selectByPrimaryKey(vid);
		v.setStatus("ref");
		vm.updateByPrimaryKey(v);
		HttpSession hs=request.getSession();
		Long id=(Long)hs.getAttribute("id");
		List L=vm.selectByExample(new VacationExample());
		List L2=new ArrayList();
		for(int i=0;i<L.size();i++)
		{
			v=(Vacation)L.get(i);
			if(v.getManagerId().equals(id))
			{
				L2.add(v);
			}
		}
		request.setAttribute("allVacation", L2);
		return "shenhe";
	}
	@RequestMapping("/deleteEmail")
	public String deleteEmail(HttpServletRequest request, Model model) throws IllegalStateException, IOException 
	{
		Long id=Long.parseLong(request.getParameter("id"));
		request.setAttribute("id", id);
		Long emailId=Long.parseLong(request.getParameter("emailId"));
		EmailExample ee=new EmailExample();
	        Email  e2=em.selectByPrimaryKey(emailId);
	        e2.setDeleted((short)1);
	        em.updateByPrimaryKey(e2);
	     List<Email> L=em.selectByExample(ee);
	     List getEmail=new ArrayList();
			for(int i=0;i<L.size();i++)
			{
				Email e=(Email)L.get(i);
				if(e.getReceiverId()==id&&(e.getDeleted()==null||e.getDeleted()==(short)0))
					getEmail.add(e);
			}
		request.setAttribute("getEmail", getEmail);
		return "getEmail";
	}
	@RequestMapping("/returnGet")
	public String returnGet(HttpServletRequest request, Model model) throws IllegalStateException, IOException 
	{
		HttpSession hs=request.getSession();
		Long id=(Long) hs.getAttribute("id");
		//request.setAttribute("id", id);
		Long emailId=Long.parseLong(request.getParameter("emailId"));
		EmailExample ee=new EmailExample();
	        Email  e2=em.selectByPrimaryKey(emailId);
	        e2.setDeleted((short)0);
	        em.updateByPrimaryKey(e2);
	     List<Email> L=em.selectByExample(ee);
	     List rubbishEmail=new ArrayList();
			for(int i=0;i<L.size();i++)
			{
				Email e=(Email)L.get(i);
				if(e.getReceiverId()==id&&(e.getDeleted()==(short)1))
					rubbishEmail.add(e);
			}
		request.setAttribute("rubbishEmail", rubbishEmail);
		return "rubbishEmail";
	}
	@RequestMapping("/destroyEmail")
	public String destroyEmail(HttpServletRequest request, Model model) throws IllegalStateException, IOException 
	{
		HttpSession hs=request.getSession();
		Long id=(Long) hs.getAttribute("id");
		//Long id=Long.parseLong(request.getParameter("id"));
		request.setAttribute("id", id);
		Long emailId=Long.parseLong(request.getParameter("emailId"));
	        em.deleteByPrimaryKey(emailId);
	     List<Email> L=em.selectByExample(new EmailExample());
	     List rubbishEmail=new ArrayList();
			for(int i=0;i<L.size();i++)
			{
				Email e=(Email)L.get(i);
				if(e.getReceiverId()==id&&(e.getDeleted()==(short)1))
					rubbishEmail.add(e);
			}
		request.setAttribute("rubbishEmail", rubbishEmail);
		return "rubbishEmail";
	}
	@RequestMapping("/returnAccounts")
	public String returnAccounts(HttpServletRequest request, Model model) 
	{
		HttpSession session=request.getSession();
		Long id=(Long)session.getAttribute("id");
		List<OA_User> L=oum.selectByExample(new OA_UserExample());
		//request.setAttribute("id",id);
		request.setAttribute("Accounts",L);
		return "Accounts";
	}
}