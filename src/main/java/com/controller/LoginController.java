package com.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.model.*;
import com.dao.*;

@Controller
@RequestMapping("/haha")
public class LoginController {
	@Resource
	private OA_UserMapper OaUserMapper;
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request,Model model){
		
		return "redirect:/";
	}
	@RequestMapping("/login")
	public String login(HttpServletRequest request, Model model) {
		String username=request.getParameter("username");
		//System.out.println("username="+username);
		String password=request.getParameter("password");
		//System.out.println("password="+password);
		String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		List<OA_User> L=OaUserMapper.selectByExample(new OA_UserExample());
		List<OA_User> L2=new ArrayList<OA_User>();
		for(int i=0;i<L.size();i++)
		{
			OA_User ou=L.get(i);
			if(ou.getJobno().equals("1"))
			{
				L2.add(ou);
			}
		}
		for(int i=0;i<L.size();i++)
		{
			OA_User ou=L.get(i);
			if(ou.getUsername().equals(username)&&ou.getPassword().equals(password))
			{
				HttpSession hs=request.getSession();
				hs.setAttribute("person",ou);
				hs.setAttribute("id",ou.getId());
				hs.setAttribute("username", ou.getUsername());
				//request.setAttribute("person",ou);
				hs.setAttribute("users", L);
				hs.setAttribute("managers", L2);
				return "successLogin";
			}
		}
		return "redirect:/";
	}
}