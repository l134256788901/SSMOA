package com.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.model.*;
import com.dao.*;

@Controller
@RequestMapping("/haha")
public class AddController {
	@Resource
	private HahaMapper hm;
	@Resource
	private OA_UserMapper oum;
	@Resource
	private EmailMapper em;
	@Resource
	private VacationMapper vm;
	@RequestMapping("/addAccount")
	public String addAccount(HttpServletRequest request, Model model) 
	{
		Long max=1L;
        List userList=oum.selectByExample(new OA_UserExample());
        if(userList==null||userList.size()==0)
        	max=0L;
        else
        {
        	for(int j=0;j<userList.size();j++)
        	{
        		OA_User ee=(OA_User)userList.get(j);
        		if(ee.getId()>max)
        			max=ee.getId();
        	}
        }
     
		String password=request.getParameter("password");
		String username=request.getParameter("username");
		Short age=Short.parseShort(request.getParameter("age"));
		String sex=request.getParameter("sex");
		String phone=request.getParameter("phone");
		String address=request.getParameter("address");
		String jobno=request.getParameter("jobno");
		OA_User ou=new OA_User();
		ou.setId(max+1);
		ou.setAddress(address);
		ou.setAge(age);
		ou.setJobno(jobno);
		ou.setPassword(password);
		ou.setPhone(phone);
		ou.setSex(sex);
		ou.setType("yy");
		ou.setUsername(username);
		oum.insert(ou);
		return "AddAccount";
	}
	@RequestMapping("/writeEmail")
	public String writeEmail(HttpServletRequest request, Model model) throws IllegalStateException, IOException 
	{
		Long id=Long.parseLong(request.getParameter("id"));
		request.setAttribute("id", id);
		 CommonsMultipartResolver cmr = new CommonsMultipartResolver(request.getServletContext());  
	        
	        String path2 = request.getContextPath();  
	        String basePath = request.getScheme() + "://"  
	                + request.getServerName() + ":" + request.getServerPort()  
	                + path2 + "/";  
	        Email  e=new Email();
	        e.setAttachment("");
	        e.setContent((String)request.getParameter("content"));
	        e.setReadstatus("no");
	        e.setReceivestatus("no");
	        e.setSendat(new Date());
	        e.setDeleted((short)0);
	        e.setReceiverId(Long.parseLong((String)request.getParameter("receiver")));
	        e.setSenderId(id);
	        e.setTitle((String)request.getParameter("title"));
	        Long max=1L;
	        List emailList=em.selectByExample(new EmailExample());
	        if(emailList==null||emailList.size()==0)
	        	max=0L;
	        else
	        {
	        	for(int j=0;j<emailList.size();j++)
	        	{
	        		Email ee=(Email)emailList.get(j);
	        		if(ee.getId()>max)
	        			max=ee.getId();
	        	}
	        }
	        e.setId(max+1L);
	        if (cmr.isMultipart(request)) {  
	            MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) (request);  
	            Iterator<String> files = mRequest.getFileNames();  
	            while (files.hasNext()) {  
	                MultipartFile mFile = mRequest.getFile(files.next());  
	                if (mFile != null) {  
	                    //String fileName = UUID.randomUUID()  
	                           //+ mFile.getOriginalFilename();  
	                    String fileName =mFile.getOriginalFilename();  
	                    ServletContext sc = request.getSession().getServletContext();  
	                    String path =sc.getRealPath("/")+fileName;  
	                   
	                    System.out.println(path);
	                    File localFile = new File(path);  
	                   
	                    mFile.transferTo(localFile);  
	                    request.setAttribute("fileUrl", fileName);  
	                    System.out.println("basePath="+basePath);
	                    System.out.println(basePath+fileName);
	                    e.setAttachment(fileName);
	                }  
	            }  
	        }  
	        em.insert(e);
	        List L=oum.selectByExample(new OA_UserExample());
	    request.setAttribute("receivers",L);
		return "writeEmail";
	}
	
	@RequestMapping("/writeQingJia")
	public String writeQingJia(HttpServletRequest request, Model model) throws IllegalStateException, IOException 
	{
		//Long id=Long.parseLong(request.getParameter("id"));
		//request.setAttribute("id", id);
	    HttpSession hs=request.getSession();
	    String year1=request.getParameter("year");
	    String month1=request.getParameter("month");
	    String day1=request.getParameter("day");
	    String year2=request.getParameter("year2");
	    String month2=request.getParameter("month2");
	    String day2=request.getParameter("day2");
		Long id=(Long)hs.getAttribute("id");
		Date startDate=new Date(Integer.parseInt(year1)-1900,Integer.parseInt(month1)-1,Integer.parseInt(day1));
		Date endDate=new Date(Integer.parseInt(year2)-1900,Integer.parseInt(month2)-1,Integer.parseInt(day2));
	        Long max=1L;
	        Vacation e=new Vacation();
	        e.setEnddate(endDate);
	        e.setManagerId(Long.parseLong((String)request.getParameter("manager")));
	        e.setReason(request.getParameter("reason"));
	        e.setStartdate(startDate);
	        e.setStatus("no");
	        e.setSupplierId(id);
	        
	        List vacationList=vm.selectByExample(new VacationExample());
	        if(vacationList==null||vacationList.size()==0)
	        	max=0L;
	        else
	        {
	        	for(int j=0;j<vacationList.size();j++)
	        	{
	        		Vacation ee=(Vacation)vacationList.get(j);
	        		if(ee.getId()>max)
	        			max=ee.getId();
	        	}
	        }
	        e.setId(max+1L);
	       
	        vm.insert(e);
	        List L=oum.selectByExample(new OA_UserExample());
	        List L2=new ArrayList();
			for(int i=0;i<L.size();i++)
			{
				OA_User ou=(OA_User)L.get(i);
				if(ou.getJobno().equals("1"))
				{
					L2.add(ou);
				}
			}
			request.setAttribute("managers", L2);
		return "qingJia";
	}
}