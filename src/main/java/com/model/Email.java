package com.model;

import java.util.Date;

public class Email {
    private Long id;

    private String title;

    private Long senderId;

    private Long receiverId;

    private String attachment;

    private Date sendat;

    private String receivestatus;

    private String readstatus;

    private Short deleted;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment == null ? null : attachment.trim();
    }

    public Date getSendat() {
        return sendat;
    }

    public void setSendat(Date sendat) {
        this.sendat = sendat;
    }

    public String getReceivestatus() {
        return receivestatus;
    }

    public void setReceivestatus(String receivestatus) {
        this.receivestatus = receivestatus == null ? null : receivestatus.trim();
    }

    public String getReadstatus() {
        return readstatus;
    }

    public void setReadstatus(String readstatus) {
        this.readstatus = readstatus == null ? null : readstatus.trim();
    }

    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}