package com.dao;

import com.model.OA_User;
import com.model.OA_UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OA_UserMapper {
    int countByExample(OA_UserExample example);

    int deleteByExample(OA_UserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OA_User record);

    int insertSelective(OA_User record);

    List<OA_User> selectByExample(OA_UserExample example);

    OA_User selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OA_User record, @Param("example") OA_UserExample example);

    int updateByExample(@Param("record") OA_User record, @Param("example") OA_UserExample example);

    int updateByPrimaryKeySelective(OA_User record);

    int updateByPrimaryKey(OA_User record);
}