package com.dao;

import com.model.Haha;
import com.model.HahaExample;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HahaMapper {
    int countByExample(HahaExample example);

    int deleteByExample(HahaExample example);

    int deleteByPrimaryKey(BigDecimal id);

    int insert(Haha record);

    int insertSelective(Haha record);

    List<Haha> selectByExample(HahaExample example);

    Haha selectByPrimaryKey(BigDecimal id);

    int updateByExampleSelective(@Param("record") Haha record, @Param("example") HahaExample example);

    int updateByExample(@Param("record") Haha record, @Param("example") HahaExample example);

    int updateByPrimaryKeySelective(Haha record);

    int updateByPrimaryKey(Haha record);
}