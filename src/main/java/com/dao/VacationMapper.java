package com.dao;

import com.model.Vacation;
import com.model.VacationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VacationMapper {
    int countByExample(VacationExample example);

    int deleteByExample(VacationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Vacation record);

    int insertSelective(Vacation record);

    List<Vacation> selectByExample(VacationExample example);

    Vacation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Vacation record, @Param("example") VacationExample example);

    int updateByExample(@Param("record") Vacation record, @Param("example") VacationExample example);

    int updateByPrimaryKeySelective(Vacation record);

    int updateByPrimaryKey(Vacation record);
}